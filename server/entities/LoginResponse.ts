import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class Tokens {
	@Field((type) => String)
	accessToken: string;

	@Field((type) => String)
	refreshToken: string;
}

@ObjectType()
export default class LoginResponse {
	@Field((type) => Boolean)
	success: boolean;

	@Field((type) => Tokens, {
		nullable: true,
	})
	tokens?: Tokens;

	@Field((type) => String, {
		nullable: true,
	})
	error?: string;
}
