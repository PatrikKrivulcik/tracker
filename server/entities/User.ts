import { Field, ObjectType, registerEnumType } from "type-graphql";
import {
	Entity,
	Property,
	PrimaryKey,
	Enum,
	ManyToMany,
	Collection,
	Cascade,
	OneToMany,
} from "@mikro-orm/core";

import { v4 } from "uuid";
import "reflect-metadata";
import Conversation from "./Conversation";
import Message from "./Message";

enum UserPermissions {
	OWNER = "owner",
	ADMIN = "admin",
	USER = "user",
}

registerEnumType(UserPermissions, {
	name: "UserPermissions",
});

@ObjectType()
@Entity()
export default class User {
	@Field((type) => String)
	@PrimaryKey()
	id: string = v4();

	@Field((type) => String!)
	@Property()
	username: string;

	@Field((type) => String!)
	@Property()
	email: string;

	@Field((type) => String!)
	@Property()
	password: string;

	@Field((type) => String, {
		nullable: true,
	})
	@Property({
		nullable: true,
	})
	refreshToken?: string;

	@Field((type) => [UserPermissions], {
		nullable: true,
	})
	@Enum({
		items: () => UserPermissions,
		array: true,
		default: [UserPermissions.USER],
	})
	permissions: UserPermissions[] = [UserPermissions.USER];

	@Field((type) => [Conversation])
	@ManyToMany(() => Conversation, (con) => con.participants, {
		cascade: [Cascade.ALL],
	})
	conversations: Collection<Conversation> = new Collection<Conversation>(
		this
	);

	@OneToMany(() => Message, (message) => message.sender)
	messages: Collection<Message> = new Collection<Message>(this);
}
