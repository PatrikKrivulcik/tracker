import { ObjectType, Field } from "type-graphql";

@ObjectType()
export default class ConversationResponse {
	@Field((type) => String)
	descriptiveName: string;

	@Field((type) => String)
	id: string;

	@Field((type) => Boolean)
	isGroup: boolean;
}
