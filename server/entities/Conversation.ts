import {
	Entity,
	Property,
	PrimaryKey,
	ManyToMany,
	Collection,
	Cascade,
	OneToMany,
} from "@mikro-orm/core";
import { ObjectType, Field } from "type-graphql";

import { v4 } from "uuid";
import User from "../entities/User";
import Message from "./Message";

@ObjectType()
@Entity()
export default class Conversation {
	@Field((type) => String)
	@PrimaryKey()
	id: string = v4();

	@Field((type) => Date)
	@Property()
	created: Date = new Date();

	@Field((type) => [User], { nullable: true })
	@ManyToMany(() => User, (user) => user.conversations, {
		owner: true,
		cascade: [Cascade.ALL],
	})
	participants: Collection<User> = new Collection<User>(this);

	@Field((type) => [Message], { nullable: true })
	@OneToMany(() => Message, (message) => message.conversation, {
		nullable: true,
	})
	messages: Collection<Message> = new Collection<Message>(this);
}
