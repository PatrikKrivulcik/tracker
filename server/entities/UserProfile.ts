import { ObjectType, Field } from "type-graphql";

@ObjectType()
export default class UserProfile {
	@Field((type) => String)
	username: string;

	@Field((type) => String)
	email: string;

	@Field((type) => String)
	organization: string = "(not implemented)";
}
