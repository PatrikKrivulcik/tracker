import { ObjectType, Field, Int } from "type-graphql";
import {
	Entity,
	Property,
	PrimaryKey,
	ManyToOne,
	Cascade,
} from "@mikro-orm/core";

import Conversation from "../entities/Conversation";
import User from "../entities/User";

@ObjectType()
@Entity()
export default class Message {
	@Field((type) => Int, { nullable: true })
	@PrimaryKey()
	id: number;

	@Field((type) => User)
	@ManyToOne(() => User, { cascade: [Cascade.ALL] })
	sender: User;

	@Field((type) => Conversation)
	@ManyToOne(() => Conversation)
	conversation: Conversation;

	@Field((type) => String)
	@Property()
	content: String;

	@Field((type) => Date)
	@Property()
	sentAt: Date = new Date();
}
