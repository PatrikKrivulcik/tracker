import User from "./entities/User";
import Conversation from "./entities/Conversation";
import Message from "./entities/Message";

export default {
	entities: [User, Conversation, Message],
	dbName: "tracker",
	type: "postgresql", // one of `mongo` | `mysql` | `mariadb` | `postgresql` | `sqlite`
	user: "tracker",
	password: "tracker",
	host: "localhost",
	port: 5432,
};
