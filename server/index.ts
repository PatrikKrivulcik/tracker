import express from "express";

import { ApolloServer } from "apollo-server-express";
import { buildSchemaSync } from "type-graphql";

import UserResolver from "./resolvers/UserResolver";
import ChatResolver from "./resolvers/ChatResolver";
const app = express();

const schema = buildSchemaSync({
	resolvers: [UserResolver, ChatResolver],
	emitSchemaFile: true,
	authChecker: ({ root, args, context, info }) => {
		return true;
	},
});

const apollo = new ApolloServer({
	schema,
	context: async ({ req }) => {
		const accessToken = req.headers.authorization?.split("Bearer ")[1];

		return {
			request: req,
			accessToken: accessToken,
		};
	},
});

apollo.applyMiddleware({ app });

app.get("/", (req, res) => {
	res.send("Hello world!");
});

app.listen(4000, () => {
	console.log("Server started");
});
