const fs = require('fs');
const path = require('path');

module.exports.loginUser = fs.readFileSync(path.join(__dirname, 'loginUser.gql'), 'utf8');
module.exports.registerUser = fs.readFileSync(path.join(__dirname, 'registerUser.gql'), 'utf8');
module.exports.sendMessage = fs.readFileSync(path.join(__dirname, 'sendMessage.gql'), 'utf8');
module.exports.startConversation = fs.readFileSync(path.join(__dirname, 'startConversation.gql'), 'utf8');
