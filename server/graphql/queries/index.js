const fs = require('fs');
const path = require('path');

module.exports.getConversations = fs.readFileSync(path.join(__dirname, 'getConversations.gql'), 'utf8');
module.exports.getMessages = fs.readFileSync(path.join(__dirname, 'getMessages.gql'), 'utf8');
module.exports.getProfile = fs.readFileSync(path.join(__dirname, 'getProfile.gql'), 'utf8');
module.exports.test = fs.readFileSync(path.join(__dirname, 'test.gql'), 'utf8');
