import { Resolver, Query, Mutation, Arg, Ctx } from "type-graphql";
import { Request } from "express";
import jwt from "jsonwebtoken";
import orm from "../orm";
import Conversation from "../entities/Conversation";
import User from "../entities/User";
import ConversationResponse from "../entities/ConversationResponse";
import Message from "../entities/Message";

@Resolver()
export default class ChatResolver {
	@Query((retuns) => [ConversationResponse])
	async getConversations(
		@Ctx("accessToken") accessToken: string,
		@Ctx("request") request: Request
	) {
		const userId = (jwt.verify(accessToken, request.ip) as any).userId;
		const em = (await orm).em;
		const user = await em.findOneOrFail(User, { id: userId }, [
			"conversations.participants",
		]);
		const conversations = user.conversations.getItems();
		const formattedConversations = conversations.map((conv) => {
			const res = new ConversationResponse();
			res.isGroup = conv.participants.length > 2;
			res.id = conv.id;

			if (res.isGroup) {
				res.descriptiveName = "Group";
			} else {
				res.descriptiveName = conv.participants
					.getItems()
					.filter((u) => {
						return u.id !== userId;
					})[0].username;
			}
			return res;
		});

		return formattedConversations;
	}

	@Mutation((returns) => Boolean)
	async startConversation(
		@Arg("secondParticipantId", {
			defaultValue: "00244732-7c11-4136-b7ae-7f90e92002cf",
		})
		secondParticipantId: string = "00244732-7c11-4136-b7ae-7f90e92002cf"
	) {
		const userId = "90cd2915-22fd-41af-a43b-d48b1899e985";
		const em = (await orm).em;

		const participants = await em.find(User, {
			id: {
				$in: [userId, secondParticipantId],
			},
		});

		if (participants.length === 2) {
			const conversation = new Conversation();
			conversation.participants.add(participants[0]);
			conversation.participants.add(participants[1]);

			em.persistAndFlush(conversation);

			return true;
		} else {
			throw Error("Seems like i fucked up ids");
		}
	}

	@Query((returns) => [Message])
	async getMessages(@Arg("conversation") conversationId: string) {
		const em = (await orm).em;

		const conversation = await em.findOneOrFail(
			Conversation,
			{
				id: conversationId,
			},
			["messages"]
		);
		const messages = conversation.messages?.getItems();
		return messages;
	}

	@Mutation((retruns) => Message)
	async sendMessage(
		@Arg("conversation") conversationId: string,
		@Arg("message") messageContent: string,
		@Ctx("accessToken") accessToken: string,
		@Ctx("request") request: Request
	) {
		const userId = (jwt.verify(accessToken, request.ip) as any).userId;
		const em = (await orm).em;

		const message = new Message();
		message.content = messageContent;
		message.sender = await em.findOneOrFail(User, { id: userId });
		message.conversation = await em.findOneOrFail(Conversation, {
			id: conversationId,
		});

		em.persistAndFlush(message);

		console.log(message);

		return message;
	}
}
