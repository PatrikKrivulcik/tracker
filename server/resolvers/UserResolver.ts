import { query, Request } from "express";
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	Authorized,
	ObjectType,
} from "type-graphql";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import User from "../entities/User";
import LoginResponse, { Tokens } from "../entities/LoginResponse";
import UserProfile from "../entities/UserProfile";

import orm from "../orm";

require("dotenv").config();

const generateTokens = (userId: string, key: string = "hehe"): Tokens => {
	return {
		accessToken: jwt.sign(
			{
				exp:
					Math.floor(Date.now() / 1000) +
					parseInt(process.env.ACCESS_TOKEN_EXPIRATION!),
				userId: userId,
			},
			key
		),
		refreshToken: jwt.sign(
			{
				exp:
					Math.floor(Date.now() / 1000) +
					parseInt(process.env.REFRESH_TOKEN_EXPIRATION!),
				userId: userId,
			},
			key
		),
	};
};

export default class UserResolver {
	@Query((returns) => Boolean)
	async test() {
		return true;
	}

	@Mutation((returns) => LoginResponse)
	async registerUser(
		@Arg("email") email: string,
		@Arg("password") password: string,
		@Arg("username") username: string,
		@Ctx("request") request: Request
	) {
		const em = (await orm).em;
		const user = new User();
		user.email = email;
		user.username = username;
		user.password = await bcrypt.hash(password, 2);
		const tokens = generateTokens(user.id, request.ip);
		user.refreshToken = tokens.refreshToken;
		em.persistAndFlush(user);

		const response = new LoginResponse();
		response.success = true;
		response.tokens = tokens;

		return response;
	}
	@Mutation((returns) => LoginResponse)
	async loginUser(
		@Arg("email") email: string,
		@Arg("password") password: string,
		@Ctx("request") request: Request
	) {
		const em = (await orm).em;
		const user = await em.findOne(User, { email: email });

		const res = new LoginResponse();
		if (user && (await bcrypt.compare(password, user.password))) {
			res.success = true;
			const tokens = generateTokens(user.id, request.ip);
			res.tokens = tokens;
			user.refreshToken = tokens.refreshToken;
			em.persistAndFlush(user);
		} else {
			res.success = false;
			res.error = "Wrong email or password";
		}

		return res;
	}

	@Query((returns) => UserProfile)
	async getProfile(
		@Ctx("accessToken") accessToken: string,
		@Ctx("request") request: Request
	) {
		const decodedToken = jwt.verify(accessToken, request.ip) as Object & {
			userId: string;
		};
		const em = (await orm).em;
		const user = await em.findOneOrFail(User, {
			id: decodedToken.userId,
		});

		const profile = new UserProfile();
		profile.email = user.email;
		profile.username = user.username;
		return profile;
	}
}
