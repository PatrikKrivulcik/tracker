import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
};

export type Conversation = {
  __typename?: 'Conversation';
  created: Scalars['DateTime'];
  id: Scalars['String'];
  messages?: Maybe<Array<Message>>;
  participants?: Maybe<Array<User>>;
};

export type ConversationResponse = {
  __typename?: 'ConversationResponse';
  descriptiveName: Scalars['String'];
  id: Scalars['String'];
  isGroup: Scalars['Boolean'];
};


export type LoginResponse = {
  __typename?: 'LoginResponse';
  error?: Maybe<Scalars['String']>;
  success: Scalars['Boolean'];
  tokens?: Maybe<Tokens>;
};

export type Message = {
  __typename?: 'Message';
  content: Scalars['String'];
  conversation: Conversation;
  id?: Maybe<Scalars['Int']>;
  sender: User;
  sentAt: Scalars['DateTime'];
};

export type Mutation = {
  __typename?: 'Mutation';
  loginUser: LoginResponse;
  registerUser: LoginResponse;
  sendMessage: Message;
  startConversation: Scalars['Boolean'];
};


export type MutationLoginUserArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationRegisterUserArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
  username: Scalars['String'];
};


export type MutationSendMessageArgs = {
  conversation: Scalars['String'];
  message: Scalars['String'];
};


export type MutationStartConversationArgs = {
  secondParticipantId?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  getConversations: Array<ConversationResponse>;
  getMessages: Array<Message>;
  getProfile: UserProfile;
  test: Scalars['Boolean'];
};


export type QueryGetMessagesArgs = {
  conversation: Scalars['String'];
};

export type Tokens = {
  __typename?: 'Tokens';
  accessToken: Scalars['String'];
  refreshToken: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  conversations: Array<Conversation>;
  email: Scalars['String'];
  id: Scalars['String'];
  password: Scalars['String'];
  permissions?: Maybe<Array<UserPermissions>>;
  refreshToken?: Maybe<Scalars['String']>;
  username: Scalars['String'];
};

export enum UserPermissions {
  Admin = 'ADMIN',
  Owner = 'OWNER',
  User = 'USER'
}

export type UserProfile = {
  __typename?: 'UserProfile';
  email: Scalars['String'];
  organization: Scalars['String'];
  username: Scalars['String'];
};

export type LoginUserMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginUserMutation = (
  { __typename?: 'Mutation' }
  & { loginUser: (
    { __typename?: 'LoginResponse' }
    & Pick<LoginResponse, 'error' | 'success'>
    & { tokens?: Maybe<(
      { __typename?: 'Tokens' }
      & Pick<Tokens, 'accessToken' | 'refreshToken'>
    )> }
  ) }
);

export type RegisterUserMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
  username: Scalars['String'];
}>;


export type RegisterUserMutation = (
  { __typename?: 'Mutation' }
  & { registerUser: (
    { __typename?: 'LoginResponse' }
    & Pick<LoginResponse, 'error' | 'success'>
    & { tokens?: Maybe<(
      { __typename?: 'Tokens' }
      & Pick<Tokens, 'accessToken' | 'refreshToken'>
    )> }
  ) }
);

export type SendMessageMutationVariables = Exact<{
  conversation: Scalars['String'];
  message: Scalars['String'];
}>;


export type SendMessageMutation = (
  { __typename?: 'Mutation' }
  & { sendMessage: (
    { __typename?: 'Message' }
    & Pick<Message, 'content' | 'id' | 'sentAt'>
    & { conversation: (
      { __typename?: 'Conversation' }
      & Pick<Conversation, 'created' | 'id'>
      & { messages?: Maybe<Array<(
        { __typename?: 'Message' }
        & Pick<Message, 'content' | 'id' | 'sentAt'>
        & { conversation: (
          { __typename?: 'Conversation' }
          & Pick<Conversation, 'created' | 'id'>
          & { participants?: Maybe<Array<(
            { __typename?: 'User' }
            & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
            & { conversations: Array<(
              { __typename?: 'Conversation' }
              & Pick<Conversation, 'created' | 'id'>
              & { messages?: Maybe<Array<(
                { __typename?: 'Message' }
                & Pick<Message, 'content' | 'id' | 'sentAt'>
                & { sender: (
                  { __typename?: 'User' }
                  & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
                  & { conversations: Array<(
                    { __typename?: 'Conversation' }
                    & Pick<Conversation, 'created' | 'id'>
                    & { participants?: Maybe<Array<(
                      { __typename?: 'User' }
                      & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
                    )>> }
                  )> }
                ) }
              )>> }
            )> }
          )>> }
        ) }
      )>> }
    ), sender: (
      { __typename?: 'User' }
      & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
    ) }
  ) }
);

export type StartConversationMutationVariables = Exact<{
  secondParticipantId?: Maybe<Scalars['String']>;
}>;


export type StartConversationMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'startConversation'>
);

export type GetConversationsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetConversationsQuery = (
  { __typename?: 'Query' }
  & { getConversations: Array<(
    { __typename?: 'ConversationResponse' }
    & Pick<ConversationResponse, 'descriptiveName' | 'id' | 'isGroup'>
  )> }
);

export type GetMessagesQueryVariables = Exact<{
  conversation: Scalars['String'];
}>;


export type GetMessagesQuery = (
  { __typename?: 'Query' }
  & { getMessages: Array<(
    { __typename?: 'Message' }
    & Pick<Message, 'content' | 'id' | 'sentAt'>
    & { conversation: (
      { __typename?: 'Conversation' }
      & Pick<Conversation, 'created' | 'id'>
      & { messages?: Maybe<Array<(
        { __typename?: 'Message' }
        & Pick<Message, 'content' | 'id' | 'sentAt'>
        & { conversation: (
          { __typename?: 'Conversation' }
          & Pick<Conversation, 'created' | 'id'>
          & { participants?: Maybe<Array<(
            { __typename?: 'User' }
            & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
            & { conversations: Array<(
              { __typename?: 'Conversation' }
              & Pick<Conversation, 'created' | 'id'>
              & { messages?: Maybe<Array<(
                { __typename?: 'Message' }
                & Pick<Message, 'content' | 'id' | 'sentAt'>
                & { sender: (
                  { __typename?: 'User' }
                  & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
                  & { conversations: Array<(
                    { __typename?: 'Conversation' }
                    & Pick<Conversation, 'created' | 'id'>
                    & { participants?: Maybe<Array<(
                      { __typename?: 'User' }
                      & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
                    )>> }
                  )> }
                ) }
              )>> }
            )> }
          )>> }
        ) }
      )>> }
    ), sender: (
      { __typename?: 'User' }
      & Pick<User, 'email' | 'id' | 'password' | 'permissions' | 'refreshToken' | 'username'>
    ) }
  )> }
);

export type GetProfileQueryVariables = Exact<{ [key: string]: never; }>;


export type GetProfileQuery = (
  { __typename?: 'Query' }
  & { getProfile: (
    { __typename?: 'UserProfile' }
    & Pick<UserProfile, 'email' | 'organization' | 'username'>
  ) }
);

export type TestQueryVariables = Exact<{ [key: string]: never; }>;


export type TestQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'test'>
);


export const LoginUserDocument = gql`
    mutation loginUser($email: String!, $password: String!) {
  loginUser(email: $email, password: $password) {
    error
    success
    tokens {
      accessToken
      refreshToken
    }
  }
}
    `;
export type LoginUserMutationFn = Apollo.MutationFunction<LoginUserMutation, LoginUserMutationVariables>;

/**
 * __useLoginUserMutation__
 *
 * To run a mutation, you first call `useLoginUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginUserMutation, { data, loading, error }] = useLoginUserMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginUserMutation(baseOptions?: Apollo.MutationHookOptions<LoginUserMutation, LoginUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginUserMutation, LoginUserMutationVariables>(LoginUserDocument, options);
      }
export type LoginUserMutationHookResult = ReturnType<typeof useLoginUserMutation>;
export type LoginUserMutationResult = Apollo.MutationResult<LoginUserMutation>;
export type LoginUserMutationOptions = Apollo.BaseMutationOptions<LoginUserMutation, LoginUserMutationVariables>;
export const RegisterUserDocument = gql`
    mutation registerUser($email: String!, $password: String!, $username: String!) {
  registerUser(email: $email, password: $password, username: $username) {
    error
    success
    tokens {
      accessToken
      refreshToken
    }
  }
}
    `;
export type RegisterUserMutationFn = Apollo.MutationFunction<RegisterUserMutation, RegisterUserMutationVariables>;

/**
 * __useRegisterUserMutation__
 *
 * To run a mutation, you first call `useRegisterUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerUserMutation, { data, loading, error }] = useRegisterUserMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *      username: // value for 'username'
 *   },
 * });
 */
export function useRegisterUserMutation(baseOptions?: Apollo.MutationHookOptions<RegisterUserMutation, RegisterUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterUserMutation, RegisterUserMutationVariables>(RegisterUserDocument, options);
      }
export type RegisterUserMutationHookResult = ReturnType<typeof useRegisterUserMutation>;
export type RegisterUserMutationResult = Apollo.MutationResult<RegisterUserMutation>;
export type RegisterUserMutationOptions = Apollo.BaseMutationOptions<RegisterUserMutation, RegisterUserMutationVariables>;
export const SendMessageDocument = gql`
    mutation sendMessage($conversation: String!, $message: String!) {
  sendMessage(conversation: $conversation, message: $message) {
    content
    conversation {
      created
      id
      messages {
        content
        conversation {
          created
          id
          participants {
            conversations {
              created
              id
              messages {
                content
                id
                sender {
                  conversations {
                    created
                    id
                    participants {
                      email
                      id
                      password
                      permissions
                      refreshToken
                      username
                    }
                  }
                  email
                  id
                  password
                  permissions
                  refreshToken
                  username
                }
                sentAt
              }
            }
            email
            id
            password
            permissions
            refreshToken
            username
          }
        }
        id
        sentAt
      }
    }
    id
    sender {
      email
      id
      password
      permissions
      refreshToken
      username
    }
    sentAt
  }
}
    `;
export type SendMessageMutationFn = Apollo.MutationFunction<SendMessageMutation, SendMessageMutationVariables>;

/**
 * __useSendMessageMutation__
 *
 * To run a mutation, you first call `useSendMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendMessageMutation, { data, loading, error }] = useSendMessageMutation({
 *   variables: {
 *      conversation: // value for 'conversation'
 *      message: // value for 'message'
 *   },
 * });
 */
export function useSendMessageMutation(baseOptions?: Apollo.MutationHookOptions<SendMessageMutation, SendMessageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SendMessageMutation, SendMessageMutationVariables>(SendMessageDocument, options);
      }
export type SendMessageMutationHookResult = ReturnType<typeof useSendMessageMutation>;
export type SendMessageMutationResult = Apollo.MutationResult<SendMessageMutation>;
export type SendMessageMutationOptions = Apollo.BaseMutationOptions<SendMessageMutation, SendMessageMutationVariables>;
export const StartConversationDocument = gql`
    mutation startConversation($secondParticipantId: String) {
  startConversation(secondParticipantId: $secondParticipantId)
}
    `;
export type StartConversationMutationFn = Apollo.MutationFunction<StartConversationMutation, StartConversationMutationVariables>;

/**
 * __useStartConversationMutation__
 *
 * To run a mutation, you first call `useStartConversationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useStartConversationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [startConversationMutation, { data, loading, error }] = useStartConversationMutation({
 *   variables: {
 *      secondParticipantId: // value for 'secondParticipantId'
 *   },
 * });
 */
export function useStartConversationMutation(baseOptions?: Apollo.MutationHookOptions<StartConversationMutation, StartConversationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<StartConversationMutation, StartConversationMutationVariables>(StartConversationDocument, options);
      }
export type StartConversationMutationHookResult = ReturnType<typeof useStartConversationMutation>;
export type StartConversationMutationResult = Apollo.MutationResult<StartConversationMutation>;
export type StartConversationMutationOptions = Apollo.BaseMutationOptions<StartConversationMutation, StartConversationMutationVariables>;
export const GetConversationsDocument = gql`
    query getConversations {
  getConversations {
    descriptiveName
    id
    isGroup
  }
}
    `;

/**
 * __useGetConversationsQuery__
 *
 * To run a query within a React component, call `useGetConversationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetConversationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetConversationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetConversationsQuery(baseOptions?: Apollo.QueryHookOptions<GetConversationsQuery, GetConversationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetConversationsQuery, GetConversationsQueryVariables>(GetConversationsDocument, options);
      }
export function useGetConversationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetConversationsQuery, GetConversationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetConversationsQuery, GetConversationsQueryVariables>(GetConversationsDocument, options);
        }
export type GetConversationsQueryHookResult = ReturnType<typeof useGetConversationsQuery>;
export type GetConversationsLazyQueryHookResult = ReturnType<typeof useGetConversationsLazyQuery>;
export type GetConversationsQueryResult = Apollo.QueryResult<GetConversationsQuery, GetConversationsQueryVariables>;
export const GetMessagesDocument = gql`
    query getMessages($conversation: String!) {
  getMessages(conversation: $conversation) {
    content
    conversation {
      created
      id
      messages {
        content
        conversation {
          created
          id
          participants {
            conversations {
              created
              id
              messages {
                content
                id
                sender {
                  conversations {
                    created
                    id
                    participants {
                      email
                      id
                      password
                      permissions
                      refreshToken
                      username
                    }
                  }
                  email
                  id
                  password
                  permissions
                  refreshToken
                  username
                }
                sentAt
              }
            }
            email
            id
            password
            permissions
            refreshToken
            username
          }
        }
        id
        sentAt
      }
    }
    id
    sender {
      email
      id
      password
      permissions
      refreshToken
      username
    }
    sentAt
  }
}
    `;

/**
 * __useGetMessagesQuery__
 *
 * To run a query within a React component, call `useGetMessagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMessagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMessagesQuery({
 *   variables: {
 *      conversation: // value for 'conversation'
 *   },
 * });
 */
export function useGetMessagesQuery(baseOptions: Apollo.QueryHookOptions<GetMessagesQuery, GetMessagesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetMessagesQuery, GetMessagesQueryVariables>(GetMessagesDocument, options);
      }
export function useGetMessagesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetMessagesQuery, GetMessagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetMessagesQuery, GetMessagesQueryVariables>(GetMessagesDocument, options);
        }
export type GetMessagesQueryHookResult = ReturnType<typeof useGetMessagesQuery>;
export type GetMessagesLazyQueryHookResult = ReturnType<typeof useGetMessagesLazyQuery>;
export type GetMessagesQueryResult = Apollo.QueryResult<GetMessagesQuery, GetMessagesQueryVariables>;
export const GetProfileDocument = gql`
    query getProfile {
  getProfile {
    email
    organization
    username
  }
}
    `;

/**
 * __useGetProfileQuery__
 *
 * To run a query within a React component, call `useGetProfileQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProfileQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetProfileQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetProfileQuery(baseOptions?: Apollo.QueryHookOptions<GetProfileQuery, GetProfileQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetProfileQuery, GetProfileQueryVariables>(GetProfileDocument, options);
      }
export function useGetProfileLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetProfileQuery, GetProfileQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetProfileQuery, GetProfileQueryVariables>(GetProfileDocument, options);
        }
export type GetProfileQueryHookResult = ReturnType<typeof useGetProfileQuery>;
export type GetProfileLazyQueryHookResult = ReturnType<typeof useGetProfileLazyQuery>;
export type GetProfileQueryResult = Apollo.QueryResult<GetProfileQuery, GetProfileQueryVariables>;
export const TestDocument = gql`
    query test {
  test
}
    `;

/**
 * __useTestQuery__
 *
 * To run a query within a React component, call `useTestQuery` and pass it any options that fit your needs.
 * When your component renders, `useTestQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTestQuery({
 *   variables: {
 *   },
 * });
 */
export function useTestQuery(baseOptions?: Apollo.QueryHookOptions<TestQuery, TestQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TestQuery, TestQueryVariables>(TestDocument, options);
      }
export function useTestLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TestQuery, TestQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TestQuery, TestQueryVariables>(TestDocument, options);
        }
export type TestQueryHookResult = ReturnType<typeof useTestQuery>;
export type TestLazyQueryHookResult = ReturnType<typeof useTestLazyQuery>;
export type TestQueryResult = Apollo.QueryResult<TestQuery, TestQueryVariables>;