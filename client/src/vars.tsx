export const API_BASE = "http://localhost:4000";

export const API_LOGIN = API_BASE + "/auth/login";
export const API_PASSIVE_LOGIN = API_BASE + "/auth/login/passive";

export const API_CONVERSATIONS = API_BASE + "/messaging/conversations";
export const API_CONVERSATIONS_SEND_MESSAGE = API_BASE + "/messaging/send";
export const API_CONVERSATIONS_GET_MESSAGES =
	API_BASE + "/messaging/conversation";
export const API_CONOVERSATION_WS = "ws://localhost:4000/";
