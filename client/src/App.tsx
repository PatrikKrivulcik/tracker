import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Projects from "./pages/Projects";
import Chat from "./pages/Chat";
import Profile from "./pages/Profile";

import "./index.scss";

const DisconnectedApp = (props: any) => {
	return (
		<BrowserRouter>
			<Switch>
				{props.userLoggedIn ? (
					<>
						<Route path="/" exact>
							<Home />
						</Route>
						<Route path="/projects">
							<Projects />
						</Route>
						<Route path="/chat" exact>
							<Chat />
						</Route>
						<Route path="/profile" exact>
							<Profile />
						</Route>
						<Route path="/">
							<Redirect to="/" />
						</Route>
					</>
				) : (
					<>
						<Route path="/register">
							<Register />
						</Route>
						<Route path="/">
							<Login />
						</Route>
					</>
				)}
			</Switch>
		</BrowserRouter>
	);
};
const App = connect((state: any) => ({
	userLoggedIn: state ? (state.login ? true : false) : false,
	accessToken: state ? state.tokens.accessToken : false,
}))(DisconnectedApp);

export default App;
