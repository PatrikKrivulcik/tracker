import React from "react";

import { Link, NavLink } from "react-router-dom";

import Header from "../components/Header";

export default function Template(props: {
	children: React.ReactNode | React.ReactNode[];
}) {
	return (
		<div className="flex flex-col h-screen overflow-hidden font-general">
			<div className="flex h-20 w-full">
				<Link
					to="/"
					className="bg-indigo-600 w-20 text-white flex items-center content-center justify-center group"
				>
					<svg
						className="w-4/12 h-4/12 group-hover:w-5/12 group-hover:h-5/12 transition-all duration-100"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z"
						></path>
					</svg>
				</Link>
				<Header />
			</div>
			<div className="flex-1 flex">
				<div className="w-20 bg-gray-800 flex flex-col p-3 text-gray-500">
					<NavLink
						to="/"
						exact={true}
						activeClassName="bg-white bg-opacity-10 text-gray-300 hover:border-0 hover:text-gray-300"
						className=" mb-2 h-14 rounded-xl flex items-center justify-center hover:text-gray-300 group"
						key={Math.random()}
					>
						<svg
							className="w-6 h-6 transform origin-center group-hover:scale-105"
							fill="none"
							stroke="currentColor"
							viewBox="0 0 24 24"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								strokeLinecap="round"
								strokeLinejoin="round"
								strokeWidth="2"
								d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
							></path>
						</svg>
					</NavLink>
					<NavLink
						to="/projects"
						exact={true}
						activeClassName="bg-white bg-opacity-10 text-gray-300 hover:border-0 hover:text-gray-300"
						className=" mb-2 h-14 rounded-xl flex items-center justify-center hover:text-gray-300 group"
						key={Math.random()}
					>
						<svg
							className="w-6 h-6 transform origin-center group-hover:scale-105"
							fill="none"
							stroke="currentColor"
							viewBox="0 0 24 24"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								strokeLinecap="round"
								strokeLinejoin="round"
								strokeWidth="2"
								d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"
							></path>
						</svg>
					</NavLink>
					<NavLink
						to="/chat"
						exact={true}
						activeClassName="bg-white bg-opacity-10 text-gray-300 hover:border-0 hover:text-gray-300"
						className=" mb-2 h-14 rounded-xl flex items-center justify-center hover:text-gray-300 group"
						key={Math.random()}
					>
						<svg
							className="w-6 h-6 transform origin-center group-hover:scale-105"
							fill="none"
							stroke="currentColor"
							viewBox="0 0 24 24"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								strokeLinecap="round"
								strokeLinejoin="round"
								strokeWidth="2"
								d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"
							></path>
						</svg>
					</NavLink>
				</div>
				<div
					className="flex-1 bg-gray-100 px-10 py-8"
					style={{
						maxHeight: "calc(100vh - 5em)",
					}}
				>
					{props.children}
				</div>
			</div>
		</div>
	);
}
