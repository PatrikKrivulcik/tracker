import Skeleton from "react-loading-skeleton";

import Template from "./Template";
import { useGetProfileQuery } from "../graphql";

export default function Profile() {
	const { data, called, loading } = useGetProfileQuery();
	console.log(loading);
	return (
		<Template>
			<div className="flex w-full items-center gap-4">
				<div className="w-32 text-indigo-500">
					<svg
						className="w-full"
						fill="currentColor"
						viewBox="0 0 20 20"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							fillRule="evenodd"
							d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
							clipRule="evenodd"
						></path>
					</svg>
				</div>
				<div className="flex-1">
					<h1 className="text-4xl font-bold w-1/3">
						{called && !loading ? (
							data?.getProfile.username
						) : (
							<Skeleton />
						)}
					</h1>
				</div>
			</div>
			<div className="flex gap-20 mt-6">
				<div className="bg-white rounded-xl flex-1 border">
					<div className="border-b px-10 py-6">
						<h2 className="text-xl font-bold text-gray-700">
							Details
						</h2>
						<p className="text-md font-normal text-gray-500">
							Personal and account details
						</p>
					</div>

					<div className="flex px-10 py-8">
						<div className="flex-1">
							<h3 className="text-sm text-gray-400">
								Organization
							</h3>
							<p>
								{called && !loading ? (
									data?.getProfile.organization || "(none)"
								) : (
									<Skeleton />
								)}
							</p>
						</div>

						<div className="flex-1">
							<h3 className="text-sm text-gray-400">Email</h3>
							<p>
								{called && !loading ? (
									data?.getProfile.email
								) : (
									<Skeleton />
								)}
							</p>
						</div>
					</div>
				</div>
				<div className="flex-1"></div>
			</div>
		</Template>
	);
}
