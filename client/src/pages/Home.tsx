import React from "react";

import Template from "./Template";

export default function Home() {
	return <Template>home</Template>;
}
