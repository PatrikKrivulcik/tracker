import React, { useState } from "react";

import Template from "./Template";
import Chatbox from "../components/Chatbox";

import { useGetConversationsQuery } from "../graphql";

export default function Chat() {
	const [loadedConversation, setLoadedConversation] = useState({
		name: "",
		id: "",
	});
	const { data, loading } = useGetConversationsQuery({
		onCompleted: (e) => {
			setLoadedConversation({
				name: e.getConversations[0].descriptiveName,
				id: e.getConversations[0].id,
			});
		},
	});
	return (
		<Template>
			<div className=" flex h-full w-full gap-10">
				<div className="w-96 flex flex-col gap-4">
					<div className="bg-indigo-500 rounded-2xl overflow-hidden">
						{loading ? (
							<div>waiting</div>
						) : (
							data!.getConversations.map((e) => {
								return (
									<div
										key={e.id}
										className="text-white flex items-center px-6 py-4 border-b border-white border-opacity-20 hover:bg-indigo-600 hover:cursor-pointer"
									>
										<div className="w-2/12">
											<svg
												className="w-full h-full"
												fill="currentColor"
												viewBox="0 0 20 20"
												xmlns="http://www.w3.org/2000/svg"
											>
												<path
													fillRule="evenodd"
													d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
													clipRule="evenodd"
												></path>
											</svg>
										</div>
										<div className="pl-3 flex-1">
											<h2>{e.descriptiveName}</h2>
											<p className="text-sm text-white text-opacity-60">
												Ahoj ako sa mas lasik?
											</p>
										</div>
										<div className="text-xs text-white text-opacity-80 text-center flex items-center gap-1">
											<div className="bg-green-400 h-2 w-2 rounded-full"></div>
											(0:00)
										</div>
									</div>
								);
							})
						)}
					</div>
				</div>
				{loadedConversation.id !== "" && (
					<Chatbox conversation={loadedConversation} />
				)}
			</div>
		</Template>
	);
}
