import React, { useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Actions } from "../scripts/Redux";

import { useLoginUserMutation } from "../graphql";

const DisconnectedLogin = (props: any) => {
	const [loginEmail, setLoginEmail] = useState("patrik@patrik.com");
	const [loginPassword, setLoginPassword] = useState("patrik");

	const [userLoginMutation, userLoginData] = useLoginUserMutation();

	return (
		<>
			<div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
				<div className="max-w-md w-full space-y-8">
					<div>
						<h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
							Sign in to your account
						</h2>
					</div>
					<form
						className="mt-8 space-y-5"
						onSubmit={async (e) => {
							e.preventDefault();
							let res: any;
							try {
								res = await userLoginMutation({
									variables: {
										password: loginPassword,
										email: loginEmail,
									},
								});
								const data = res.data?.loginUser;
								if (data?.success) {
									if (data.success) {
										props.reduxLogin(
											data.tokens?.accessToken,
											data.tokens?.refreshToken
										);
									} else {
										alert(data.error);
									}
								}
							} catch (e) {}
							console.log(userLoginData);
						}}
					>
						<input type="hidden" name="remember" value="true" />
						<div className="rounded-md shadow-sm -space-y-px">
							<div>
								<label
									htmlFor="email-address"
									className="sr-only"
								>
									Email address
								</label>
								<input
									id="email-address"
									name="email"
									type="email"
									autoComplete="email"
									className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-purple-500 focus:z-10 sm:text-sm"
									placeholder="Email address"
									onChange={(e) => {
										e.preventDefault();
										setLoginEmail(e.target.value);
									}}
								/>
							</div>
							<div>
								<label htmlFor="password" className="sr-only">
									Password
								</label>
								<input
									id="password"
									name="password"
									type="password"
									autoComplete="current-password"
									className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-purple-500 focus:z-10 sm:text-sm"
									placeholder="Password"
									onChange={(e) => {
										e.preventDefault();
										setLoginPassword(e.target.value);
									}}
								/>
							</div>
						</div>

						<div>
							<button className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
								<span className="absolute left-0 inset-y-0 flex items-center pl-3">
									{/* <!-- Heroicon name: solid/lock-closed --> */}
									<svg
										className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 20 20"
										fill="currentColor"
										aria-hidden="true"
									>
										<path
											fillRule="evenodd"
											d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
											clipRule="evenodd"
										/>
									</svg>
								</span>
								Sign in
							</button>
						</div>
						<div className="text-sm text-right">
							<Link
								to="/register"
								href="#"
								className="font-medium text-indigo-600 hover:text-indigo-500"
							>
								Create account
							</Link>
						</div>
					</form>
				</div>
			</div>
		</>
	);
};

const Login = connect(null, (dispatch) => {
	return {
		reduxLogin: (accessToken: string, refreshToken: string) =>
			dispatch(Actions.login(accessToken, refreshToken)),
	};
})(DisconnectedLogin);

export default Login;
