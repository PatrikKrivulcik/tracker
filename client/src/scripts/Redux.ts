import { createStore, Action, Store } from "redux";

enum ActionTypes {
	LOGIN_ACTION,
	LOGOUT_ACTION,
}

interface ActionType extends Action {
	type: ActionTypes;
	payload?: any;
}

interface StateType {
	login: boolean;
	tokens?: {
		accessToken: string;
		refreshToken: string;
	};
	user?: {
		username: string;
	};
}

const DefaultState: StateType = {
	login: true,
};

export const Actions: { [key: string]: (...args: any[]) => ActionType } = {
	login: (accessToken: string, refreshToken: string) => {
		window.localStorage.setItem("refreshToken", refreshToken);
		window.localStorage.setItem("accessToken", accessToken);
		return {
			type: ActionTypes.LOGIN_ACTION,
			payload: {
				accessToken: accessToken,
				refreshToken: refreshToken,
			},
		};
	},
	logout: () => {
		window.localStorage.removeItem("refreshToken");
		window.localStorage.removeItem("accessToken");

		return {
			type: ActionTypes.LOGOUT_ACTION,
		};
	},
};

function ActionReducer(
	prevState: StateType | undefined,
	action: ActionType
): StateType {
	switch (action.type) {
		case ActionTypes.LOGIN_ACTION:
			return {
				...prevState,
				login: true,
				tokens: action.payload,
			};
		case ActionTypes.LOGOUT_ACTION:
			let newState = { ...prevState, login: false };
			delete newState.user;
			delete newState.tokens;
			return {
				...newState,
			};
	}
}

const store: Store<StateType, ActionType> = createStore(
	ActionReducer,
	DefaultState
);

export default store;
