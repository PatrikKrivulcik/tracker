import React, { useState } from "react";
import { Link } from "react-router-dom";
import UseAnimations from "react-useanimations";
import searchToX from "react-useanimations/lib/searchToX";

const Header = () => {
	const [searchText, setSearchText] = useState("");
	const [notificationsPanelVisible, setNotificationsPanelVisible] = useState(
		false
	);
	return (
		<div className="flex-1 border-b borde-gray-300 flex px-10 justify-between relative">
			<div className="flex items-center">
				<UseAnimations
					animation={searchToX}
					size={16 * 2}
					strokeColor="rgba(0,0,0,0.4)"
					onClick={(e) => {
						alert(`Searching: ${searchText}`);
					}}
				/>
				<input
					type="text"
					placeholder="Search"
					className="focus:outline-none ml-2 appearance-none"
					onChange={(e) => {
						e.preventDefault();
						setSearchText(e.target.value);
					}}
				/>
			</div>
			<div className="flex">
				<button
					className="w-12 ml-2 text-gray-400 focus:outline-none"
					onClick={(e) => {
						setNotificationsPanelVisible(
							!notificationsPanelVisible
						);
					}}
				>
					<svg
						className="w-6 h-6 m-auto
								"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
						></path>
					</svg>
				</button>
				<Link
					to="/profile"
					className="w-12 ml-2 text-indigo-600 flex items-center justify-center"
				>
					<svg
						className="w-10 h-10 align-middle"
						fill="currentColor"
						viewBox="0 0 20 20"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							fillRule="evenodd"
							d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
							clipRule="evenodd"
						></path>
					</svg>
				</Link>
			</div>
			<div
				onMouseLeave={(e) => {
					setTimeout(() => {
						setNotificationsPanelVisible(false);
					}, 200);
				}}
				className={
					"top-16 bg-white rounded-xl w-72 border right-10 " +
					(notificationsPanelVisible ? "absolute" : "hidden")
				}
			>
				{[1, 1, 1, 1, 1].map(() => {
					return (
						<div
							className="border-b flex py-3 hover:bg-gray-100 hover:cursor-pointer"
							key={Math.random()}
						>
							<div className="text-indigo-500 px-3">
								<svg
									className="w-6 h-6"
									fill="currentColor"
									viewBox="0 0 20 20"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										fillRule="evenodd"
										d="M18 10c0 3.866-3.582 7-8 7a8.841 8.841 0 01-4.083-.98L2 17l1.338-3.123C2.493 12.767 2 11.434 2 10c0-3.866 3.582-7 8-7s8 3.134 8 7zM7 9H5v2h2V9zm8 0h-2v2h2V9zM9 9h2v2H9V9z"
										clipRule="evenodd"
									></path>
								</svg>
							</div>
							<div>
								<h5>Patrik Krivulcik</h5>
								<p className="text-sm leading-4 text-gray-500">
									Lorem ipsum dolor sit amet consectetur
									adipisicing elit.
								</p>
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default Header;
