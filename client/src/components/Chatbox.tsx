import React, { useEffect, useState } from "react";

import { useGetMessagesLazyQuery, useSendMessageMutation } from "../graphql";

const Chatbox = (props: {
	conversation: {
		id: string;
		name: string;
	};
}) => {
	const [messageContent, setMessageContent] = useState("");
	const [sendMessageMutation] = useSendMessageMutation();

	const [getMessages, { data, loading, error }] = useGetMessagesLazyQuery({
		variables: {
			conversation: props.conversation.id,
		},
		pollInterval: 2000,
		onCompleted: () => {
			console.log(data, error);
		},
	});

	useEffect(() => {
		getMessages({
			variables: {
				conversation: props.conversation.id,
			},
		});
	}, [props.conversation.id, getMessages]);
	return (
		<div className="flex-1 h-full rounded-2xl bg-indigo-500 flex flex-col items-stretch justify-items-stretch relative">
			<div className="flex items-center px-10 py-4 border-b border-white border-opacity-30">
				<div className="flex-1 text-xl text-white">
					{props.conversation.name}
				</div>
				<div className="text-white hover:bg-white p-3 hover:bg-opacity-10 rounded-full cursor-pointer">
					<svg
						className="w-6 h-6"
						fill="currentColor"
						viewBox="0 0 20 20"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z"></path>
					</svg>
				</div>
			</div>
			<div className="w-full flex-1 overflow-y-scroll p-10 flex flex-col">
				{!loading &&
					data?.getMessages.map((msg) => {
						return msg.sender.refreshToken ===
							window.localStorage.getItem("refreshToken") ? (
							<div
								key={msg.id}
								className="bg-gray-700 text-white max-w-xl p-4 rounded-tl-2xl rounded-tr-2xl rounded-bl-2xl br self-end"
							>
								{msg.content}
							</div>
						) : (
							<div
								key={msg.id}
								className="bg-white text-black text-opacity-70 max-w-xl p-4 rounded-tl-2xl rounded-tr-2xl rounded-br-2xl br self-start"
							>
								{msg.content}
							</div>
						);
					})}
				{/* {[1, 1, 1, 1, 1].map(() => {
					return (
						<div className="bg-white text-black text-opacity-70 max-w-xl p-4 rounded-tl-2xl rounded-tr-2xl rounded-br-2xl br self-start">
							Lorem, ipsum dolor sit amet consectetur adipisicing
							elit. Veniam quidem explicabo dolorum blanditiis.
							Facilis dolorem, vel facere architecto consequuntur
							qui beatae non alias necessitatibus unde repudiandae
							animi tempora voluptatum mollitia.
						</div>
					);
				})}
				{[1, 1, 1, 1, 1].map(() => {
					return (
						<div className="bg-gray-700 text-white max-w-xl p-4 rounded-tl-2xl rounded-tr-2xl rounded-bl-2xl br self-end">
							Lorem, ipsum dolor sit amet consectetur adipisicing
							elit. Veniam quidem explicabo dolorum blanditiis.
							Facilis dolorem, vel facere architecto consequuntur
							qui beatae non alias necessitatibus unde repudiandae
							animi tempora voluptatum mollitia.
						</div>
					);
				})} */}
			</div>
			<div className=" flex p-6 gap-6">
				<input
					type="text"
					className="flex-1 py-4 px-6 rounded-full focus:outline-none"
					placeholder="Message"
					value={messageContent}
					onChange={(e) => {
						setMessageContent(e.target.value);
					}}
				/>
				<button
					className="bg-gray-700 text-white w-16 h-16  rounded-full  hover:bg-indigo-700 focus:outline-none"
					onClick={async (e) => {
						if (messageContent !== "") {
							await sendMessageMutation({
								variables: {
									conversation: props.conversation.id,
									message: messageContent,
								},
							});
							alert(messageContent);
						}
						setMessageContent("");
					}}
				>
					<svg
						className="w-1/2 h-1/2 m-auto"
						fill="currentColor"
						viewBox="0 0 20 20"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							fillRule="evenodd"
							d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
							clipRule="evenodd"
						></path>
					</svg>
				</button>
			</div>
		</div>
	);
};

export default Chatbox;
